extern crate rand;

use std::io;
use std::ops::Sub;
use rand::Rng;

#[derive(Debug, PartialEq, Eq)]
pub enum Peg {
    White,
    Black,
    Red,
    Green,
    Orange,
    Yellow
}

#[derive(Debug, PartialEq)]
pub struct Hint {
    pub correct_colour: i8,
    pub correct_position: i8,
}

impl Hint {
    pub fn new() -> Hint {
        Hint{
            correct_colour: 0,
            correct_position: 0,
        }
    }
}

struct PegCount {
    white: i8,
    black: i8,
    red: i8,
    green: i8,
    orange: i8,
    yellow: i8,
}

impl PegCount {
    fn new() -> PegCount {
        PegCount {
            white: 0,
            black: 0,
            red: 0,
            green: 0,
            orange: 0,
            yellow: 0,
        }
    }
    fn to_vec(&self) -> Vec<i8> {
        let mut v = vec![0;6];

        v[0] = self.white;
        v[1] = self.black;
        v[2] = self.red;
        v[3] = self.green;
        v[4] = self.orange;
        v[5] = self.yellow;

        v
    }
}

impl Sub for PegCount {
    type Output = PegCount;

    fn sub(self, other: PegCount) -> PegCount {
        PegCount {
            white: self.white - other.white,
            black: self.black - other.black,
            red: self.red - other.red,
            green: self.green - other.green,
            orange: self.orange - other.orange,
            yellow: self.yellow - other.yellow,
        }
    }
}

pub fn generate_problem() -> Vec<Peg> {
    let mut pegs: Vec<Peg> = Vec::new();
    for _ in 0..4 {
        let new_peg = rand::thread_rng().gen_range(0, 6);
        pegs.push(match new_peg {
            0 => Peg::White,
            1 => Peg::Black,
            2 => Peg::Red,
            3 => Peg::Green,
            4 => Peg::Orange,
            5 => Peg::Yellow,
            _ => panic!("Random number not between 0 and 5 inclusive"),
        });
    }

    pegs
}

pub fn get_guess() -> Result<Vec<Peg>, &'static str> {
    let mut guess = String::new();
    io::stdin().read_line(&mut guess)
        .expect("Failed to read line");
    let guess = guess.trim();
    if guess == "q" || guess == "quit" { return Err("QUIT") }
    if guess == "h" || guess == "help" { return Err("HELP") }
    if guess.len() != 4 { return Err("You should only place exactly 4 pegs.") };

    let mut guess_vec: Vec<Peg> = vec![];

    for c in guess.chars() {
        guess_vec.push(match c {
            'w' => Peg::White,
            'b' => Peg::Black,
            'r' => Peg::Red,
            'g' => Peg::Green,
            'o' => Peg::Orange,
            'y' => Peg::Yellow,
            _   => return Err("One or more pegs of unknown colour."),
        })
    }

    Ok(guess_vec)
}

pub fn compare_to_solution(guess: &Vec<Peg>, solution: &Vec<Peg>) -> Hint {
    let mut hint = Hint::new();
    let mut sol_count = PegCount::new();
    let mut guess_count = PegCount::new();

    for peg in solution {
        match *peg {
            Peg::White  => sol_count.white += 1,
            Peg::Black  => sol_count.black += 1,
            Peg::Red    => sol_count.red += 1,
            Peg::Green  => sol_count.green += 1,
            Peg::Orange => sol_count.orange += 1,
            Peg::Yellow => sol_count.yellow += 1,
        };
    }

    for peg in guess {
        match *peg {
            Peg::White  => guess_count.white += 1,
            Peg::Black  => guess_count.black += 1,
            Peg::Red    => guess_count.red += 1,
            Peg::Green  => guess_count.green += 1,
            Peg::Orange => guess_count.orange += 1,
            Peg::Yellow => guess_count.yellow += 1,
        };
    }

    let difference = sol_count - guess_count;
    let difference = difference.to_vec();

    // Get total number of total correct coloured pegs
    hint.correct_colour = 4;
    for n in difference {
        match n >= 0 {
            true => hint.correct_colour -= n,
            false => (),
        }
    }

    // Get total correctly placed pegs
    for (i, _) in guess.into_iter().enumerate() {
        if guess[i] == solution[i] {
            hint.correct_position += 1;
        }
    }

    // Remove double counts (correct colour AND placement) from correct_position
    hint.correct_colour -= hint.correct_position;

    hint
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn generate_problem_test() {
        generate_problem();
    }

    #[test]
    fn four_position_guess() {
        let solution = vec![Peg::Black, Peg::Orange, Peg::Red, Peg::Yellow];

        let expected = Hint {
            correct_colour: 0,
            correct_position: 4,
        };

        assert_eq!(compare_to_solution(&solution, &solution), expected);
    }

    #[test]
    fn two_colour_two_position_guess() {
        let solution = vec![Peg::Black, Peg::Black, Peg::Red, Peg::Yellow];
        let guess = vec![Peg::Black, Peg::Red, Peg::Black, Peg::Yellow];

        let expected = Hint {
            correct_colour: 2,
            correct_position: 2,
        };

        assert_eq!(compare_to_solution(&guess, &solution), expected);
    }

    #[test]
    fn four_colour_guess() {
        let solution = vec![Peg::Black, Peg::Black, Peg::Red, Peg::Yellow];
        let guess = vec![Peg::Red, Peg::Yellow, Peg::Black, Peg::Black];

        let expected = Hint {
            correct_colour: 4,
            correct_position: 0,
        };

        assert_eq!(compare_to_solution(&guess, &solution), expected);
    }

    #[test]
    fn wrong_guess() {
        let solution = vec![Peg::Black, Peg::Black, Peg::Red, Peg::Yellow];
        let guess = vec![Peg::Orange, Peg::Orange, Peg::Orange, Peg::Orange];

        let expected = Hint {
            correct_colour: 0,
            correct_position: 0,
        };

        assert_eq!(compare_to_solution(&guess, &solution), expected);
    }
}