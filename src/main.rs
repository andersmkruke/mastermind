extern crate mastermind;

use std::process;
use std::io;

fn main() {
    let problem = mastermind::generate_problem();
    let mut num_of_guesses = 0;
    let max_guesses = 12;

    print_help();
    println!("Good luck!");

    while num_of_guesses < max_guesses
    {
        // Get input from player
        println!("\nEnter your guess #{}:", num_of_guesses+1);
        let guess = mastermind::get_guess();
        let guess = match guess {
            //Err("QUIT") => process::exit(0),
            Err(msg) if msg == "QUIT" => process::exit(0),
            Err(msg) if msg == "HELP" => {
                print_help();
                continue;
            },
            Err(msg) => {
                println!("{}", msg);
                continue;
            },
            Ok(g) => g,
        };

        println!("Guess: {:?}", guess);
        // Compare to solution - print # correct + # correct position
        let hint = mastermind::compare_to_solution(&guess, &problem);

        // If all are positioned correctly, game won
        if hint.correct_position == 4 {
            println!("Congratulations! You won!");
            break;
        } else {
            println!("Correct placement: {}", hint.correct_position);
            println!("Correct colour, but incorrect placement: {}", hint.correct_colour);
            println!("Try again!");
        }

        num_of_guesses += 1;
    }

    // Make highscore table as file


    if num_of_guesses == 12 {
        println!("Solution was: {:?}", problem);
    }

    println!("\nPress enter to exit.");
    let mut buf = String::new();
    match io::stdin().read_line(&mut buf) {
        _ => ()
    }
}

fn print_help() {
    println!("########################################################");
    println!("# Try to break the code in as few tries as possible!   #");
    println!("# The code is made up of four coloured pegs.           #");
    println!("# Make a guess by typing in their respective letters.  #");
    println!("# For example: \"yyrb\". Available colours are:          #");
    println!("# (w)hite, (b)lack, (r)ed, (g)reen, (o)range, (y)ellow #");
    println!("# 'h' or 'help' to show this text again.               #");
    println!("# 'q' or 'quit' to exit game.                          #");
    println!("########################################################");
}